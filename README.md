# Flickr photos

## Some architecture decisions

I've decided to avoid high level things like LiveData and coroutines, because it seems like the task implies some lower level implementation.

Very simple constructor-injection DI is used (it's enough for single screen app).

Simple handwritten AsyncExecutor has a lot of things in common with AsyncTask, but it is customizable and has more suitable interface. Single instance manages multiple tasks.

RecyclerView adapter with delegates is used to decompose each view type into simple atomic classes.

Bitmap loading logic is represented by 3 classes:

* BitmapDisplayer: requests bitmap and handles it's appearance in view
* BitmapLoader: manages loading tasks for all images, loads bitmap from cache and from network. Presenter holds this loader, so presenter is a single entry point for all async requests and it manages all subscriptions based on lifecycle.
* BitmapDiscCache: simple cache implementation. 

## Things to improve

* Bitmap memory cache (only disc cache is implemented currently).
* To improve bitmap disc cache to be LRU (currently it deletes the oldest images when maximum cache size is exceeded, this can lead to non optimal cache usage).
* To use some bitmap pool for input stream decoding.
* To use StaggeredGridLayoutManager and show images without cropping. It is hard to implement smoothly because Flick api doesn't provide any info about photos aspect ratio.
* To resize cached bitmaps according to ImageView size to save space (like target cache in Glide).
* Pull to refresh (currently user won't receive new uploaded images until he changes the query).
* Auto retry loading when internet is reconnected.
* More tests (as always).
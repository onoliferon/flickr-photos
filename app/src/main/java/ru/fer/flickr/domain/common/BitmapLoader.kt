package ru.fer.flickr.domain.common

import android.graphics.Bitmap
import ru.fer.flickr.data.common.AsyncExecutor
import ru.fer.flickr.data.common.AsyncListener
import ru.fer.flickr.data.common.BitmapDiscCache
import ru.fer.flickr.data.common.HttpDownloader
import java.net.URL

class BitmapLoader(
    private val asyncExecutor: AsyncExecutor,
    private val httpDownloader: HttpDownloader,
    private val discCache: BitmapDiscCache
) {
    private val currentJobs: MutableMap<Any, AsyncExecutor.Job> = mutableMapOf()

    fun loadBitmap(
        url: URL,
        key: Any,
        listener: AsyncListener<BitmapLoadingResult>
    ) {
        clear(key)

        val job = asyncExecutor.execute(
            task = { cancellationInfo ->
                val cached = discCache.get(url.toString())
                if (cached != null) {
                    return@execute BitmapLoadingResult(
                        bitmap = cached,
                        fromCache = true
                    )
                }
                val bitmap = httpDownloader.downloadImage(url)

                // Don't cache bitmap because task is interrupted and bitmap is probably corrupted
                // Workaround to avoid bug like this https://github.com/square/picasso/issues/1789
                if (!cancellationInfo.isCancelled()) {
                    discCache.put(bitmap, url.toString())
                }
                return@execute BitmapLoadingResult(
                    bitmap = bitmap,
                    fromCache = false
                )
            },
            listener = object : AsyncListener<BitmapLoadingResult> {
                override fun onSuccess(result: BitmapLoadingResult) {
                    currentJobs.remove(key)
                    listener.onSuccess(result)
                }

                override fun onError(exception: Exception) {
                    currentJobs.remove(key)
                    listener.onError(exception)
                }
            }
        )

        currentJobs[key] = job
    }

    fun clear(key: Any) {
        if (currentJobs.containsKey(key)) {
            currentJobs[key]?.cancel()
            currentJobs.remove(key)
        }
    }

    fun clearAll() {
        currentJobs.values.forEach { it.cancel() }
        currentJobs.clear()
    }

    data class BitmapLoadingResult(
        val bitmap: Bitmap,
        val fromCache: Boolean
    )
}
package ru.fer.flickr.domain.photos

class PhotoUrlFactory {
    fun getPhotoUrl(photo: Photo): String {
        return "https://farm${photo.farm}.static.flickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg"
    }
}
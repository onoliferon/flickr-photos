package ru.fer.flickr.domain.photos

data class LoadedPhotos(
    val photos: List<Photo>,
    val allLoaded: Boolean
)
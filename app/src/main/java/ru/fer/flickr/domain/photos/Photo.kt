package ru.fer.flickr.domain.photos

data class Photo (
    val id: String,
    val title: String,
    val farm: Int,
    val server: String,
    val secret: String
)
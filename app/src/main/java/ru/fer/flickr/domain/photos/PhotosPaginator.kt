package ru.fer.flickr.domain.photos

import ru.fer.flickr.data.common.AsyncExecutor
import ru.fer.flickr.data.common.AsyncListener
import ru.fer.flickr.data.photos.PhotosRepository
import java.lang.Exception

class PhotosPaginator(
    private val photosRepository: PhotosRepository,
    private val currentTimeProvider: CurrentTimeProvider,
    private val limitPerPage: Int,
    initialQuery: String?
) {

    interface CurrentTimeProvider {
        fun getCurrentTimeMillis(): Long
    }

    interface PhotosObserver {
        fun onLoaded(photos: List<Photo>, allLoaded: Boolean)

        fun onError(photos: List<Photo>, exception: Exception)
    }

    private val observers: MutableList<PhotosObserver> = mutableListOf()

    private var currentPage: Int = 0 // The first valid page is 1
    private var currentSearchQuery: String? = initialQuery
    private var allLoaded: Boolean = false
    private val currentLoadedPhotos: MutableList<Photo> = mutableListOf()
    private var currentMaxPhotoDate: Long? = null

    private var currentLoadingJob: AsyncExecutor.Job? = null

    fun getSearchQuery(): String? = currentSearchQuery

    fun getLoadedPhotos(): List<Photo> = currentLoadedPhotos

    fun resetWithSearchQuery(query: String?) {
        if (query == currentSearchQuery) {
            return
        }
        currentMaxPhotoDate = generateMaxPhotoDate()
        currentLoadingJob?.cancel()
        currentLoadingJob = null
        currentLoadedPhotos.clear()
        currentPage = 0
        currentSearchQuery = query
        allLoaded = false
    }

    fun loadMore() {
        if (currentLoadingJob != null || allLoaded) {
            return
        }

        if (currentMaxPhotoDate == null) {
            currentMaxPhotoDate = generateMaxPhotoDate()
        }
        val maxDate = currentMaxPhotoDate!!

        currentLoadingJob = photosRepository.loadPhotos(
            page = currentPage + 1,
            limit = limitPerPage,
            searchString = currentSearchQuery,
            maxUploadDateSeconds = maxDate,
            listener = object : AsyncListener<LoadedPhotos> {
                override fun onSuccess(result: LoadedPhotos) {
                    currentPage++
                    currentLoadedPhotos.addAll(result.photos)
                    allLoaded = result.allLoaded
                    observers.forEach {
                        it.onLoaded(
                            getCurrentLoadedPhotosCopy(),
                            allLoaded = allLoaded
                        )
                    }
                    currentLoadingJob = null
                }

                override fun onError(exception: Exception) {
                    observers.forEach { it.onError(getCurrentLoadedPhotosCopy(), exception) }
                    currentLoadingJob = null
                }
            }
        )
    }

    fun subscribe(observer: PhotosObserver) {
        observers.add(observer)
        observer.onLoaded(getCurrentLoadedPhotosCopy(), allLoaded = allLoaded)
    }

    fun unsubscribe(observer: PhotosObserver) {
        observers.remove(observer)
    }

    private fun generateMaxPhotoDate(): Long {
        // Current time minus 20 minutes

        // Something is wrong with Flickr api:
        // if we request images with max_upload_date == now, it still includes new
        // images during N-page loading, which causes duplicates
        return currentTimeProvider.getCurrentTimeMillis() / 1000 - 20 * 60
    }

    private fun getCurrentLoadedPhotosCopy(): List<Photo> {
        return mutableListOf<Photo>().apply { addAll(currentLoadedPhotos) }
    }
}
package ru.fer.flickr.presentation.photos.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_error.view.*
import ru.fer.flickr.R
import ru.fer.flickr.presentation.common.AdapterDelegate
import ru.fer.flickr.presentation.common.GridAdapterDelegate
import ru.fer.flickr.presentation.common.ListItem

class ErrorDelegate(
    private val retryListener: () -> Unit
) :
    AdapterDelegate<ErrorDelegate.Item, ErrorDelegate.ViewHolder>,
    GridAdapterDelegate {
    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_error, parent, false)

        view.retryButton.setOnClickListener { retryListener() }
        return ViewHolder(view)
    }

    override fun bindViewHolder(holder: ViewHolder, item: Item) {
    }

    override fun suitsFor(item: ListItem): Boolean = item is Item

    override fun takesFullWidth(): Boolean = true

    data class Item(
        override val listId: String? = null
    ) : ListItem

    class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view)
}
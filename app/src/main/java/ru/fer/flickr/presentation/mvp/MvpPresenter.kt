package ru.fer.flickr.presentation.mvp

import android.os.Bundle

abstract class MvpPresenter<V: MvpView> {

    protected var view: V? = null

    fun attachView(view: V) {
        this.view = view
        onAttachView(view)
    }

    fun detachView() {
        view = null
        onDetachView()
    }

    protected abstract fun onAttachView(view: V)

    protected abstract fun onDetachView()

    abstract fun onSaveInstanceState(state: Bundle)

    abstract fun onRestoreInstanceState(state: Bundle)
}
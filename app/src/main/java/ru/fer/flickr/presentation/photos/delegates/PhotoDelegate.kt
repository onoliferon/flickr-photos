package ru.fer.flickr.presentation.photos.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_photo.view.*
import ru.fer.flickr.R
import ru.fer.flickr.presentation.common.AdapterDelegate
import ru.fer.flickr.presentation.common.GridAdapterDelegate
import ru.fer.flickr.presentation.common.BitmapDisplayer
import ru.fer.flickr.presentation.common.ListItem
import java.net.URL

class PhotoDelegate(
    private val bitmapDisplayer: BitmapDisplayer,
    private val itemSizeIncludingPadding: Int
) : AdapterDelegate<PhotoDelegate.Item, PhotoDelegate.ViewHolder>,
    GridAdapterDelegate {

    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_photo, parent, false)
        view.layoutParams.width = itemSizeIncludingPadding
        view.layoutParams.height = itemSizeIncludingPadding
        return ViewHolder(view)
    }

    override fun bindViewHolder(holder: ViewHolder, item: Item) {
        bitmapDisplayer.displayImage(
            url = item.imageUrl,
            view = holder.imageView
        )
    }

    override fun suitsFor(item: ListItem): Boolean = item is Item

    override fun takesFullWidth(): Boolean = false

    class ViewHolder(
        view: View,
        val imageView: ImageView = view.imageView
    ) : RecyclerView.ViewHolder(view)


    data class Item(
        override val listId: String,
        val imageUrl: URL,
        val title: String
    ) : ListItem
}
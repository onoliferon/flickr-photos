package ru.fer.flickr.presentation.mvp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class MvpActivity<V : MvpView, P : MvpPresenter<V>> : AppCompatActivity() {

    private var _presenter: P? = null
    protected val presenter: P?
        get() = _presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // TODO Presenter could extend ViewModel to persist
        @Suppress("UNCHECKED_CAST")
        _presenter = lastCustomNonConfigurationInstance as? P ?: createPresenter()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        @Suppress("UNCHECKED_CAST")
        _presenter?.attachView(this as V)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        _presenter?.onRestoreInstanceState(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        _presenter?.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        _presenter?.detachView()
    }

    override fun onRetainCustomNonConfigurationInstance(): Any? {
        return _presenter
    }

    protected abstract fun createPresenter(): P
}
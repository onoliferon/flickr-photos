package ru.fer.flickr.presentation.photos

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_photos.*
import ru.fer.flickr.R
import ru.fer.flickr.data.common.HttpDownloader
import ru.fer.flickr.data.common.SimpleIOAsyncExecutor
import ru.fer.flickr.data.photos.PhotosParser
import ru.fer.flickr.data.photos.PhotosRepository
import ru.fer.flickr.data.common.BitmapDiscCache
import ru.fer.flickr.domain.common.BitmapLoader
import ru.fer.flickr.presentation.common.BitmapDisplayer
import ru.fer.flickr.domain.photos.PhotoUrlFactory
import ru.fer.flickr.domain.photos.PhotosPaginator
import ru.fer.flickr.presentation.common.DelegatesAdapter
import ru.fer.flickr.presentation.common.ListItem
import ru.fer.flickr.presentation.mvp.MvpActivity
import ru.fer.flickr.presentation.photos.delegates.EmptyResultDelegate
import ru.fer.flickr.presentation.photos.delegates.ErrorDelegate
import ru.fer.flickr.presentation.photos.delegates.LoadingDelegate
import ru.fer.flickr.presentation.photos.delegates.PhotoDelegate
import java.lang.IllegalStateException

class PhotosActivity : MvpActivity<PhotosView, PhotosPresenter>(), PhotosView {

    companion object {
        private const val SPAN_COUNT = 3
        private const val PHOTO_PER_PAGE = 12
        private const val INPUT_TEXT_DEBOUNCE_DURATION = 300L
    }

    private lateinit var adapter: DelegatesAdapter
    private val layoutManager = GridLayoutManager(this, SPAN_COUNT)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)

        adapter = DelegatesAdapter(
            listOf(
                PhotoDelegate(
                    bitmapDisplayer = BitmapDisplayer(
                        presenter?.getBitmapLoader()
                            ?: throw IllegalStateException("Presenter must be initialized")
                    ),
                    itemSizeIncludingPadding = calculateItemSize()
                ),
                LoadingDelegate(),
                ErrorDelegate(retryListener = {
                    presenter?.retryLoadMorePhotos()
                }),
                EmptyResultDelegate()
            )
        )

        layoutManager.spanSizeLookup = adapter.createSpanSizeLookup(spanCount = SPAN_COUNT)
        layoutManager.initialPrefetchItemCount = SPAN_COUNT * 2

        photosRecyclerView.setHasFixedSize(true)
        photosRecyclerView.layoutManager = layoutManager
        photosRecyclerView.adapter = adapter
        photosRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    loadMoreIfNeeded()
                }
            }
        })

        photosRecyclerView.addOnLayoutChangeListener { _, _, top, _, bottom, _, oldTop, _, oldBottom ->
            if (top != oldTop || bottom != oldBottom) {
                loadMoreIfNeeded()
            }
        }

        setupSearchQueryListener()
    }

    override fun createPresenter(): PhotosPresenter {
        // The simplest DI approach is enough in this case
        val executor = SimpleIOAsyncExecutor()
        val httpDownloader = HttpDownloader()
        return PhotosPresenter(
            photosPaginator = PhotosPaginator(
                PhotosRepository(
                    executor,
                    httpDownloader,
                    PhotosParser()
                ),
                currentTimeProvider = CurrentTimeProvider(),
                limitPerPage = PHOTO_PER_PAGE,
                initialQuery = getString(R.string.searchDefaultValue)
            ),
            photoUrlFactory = PhotoUrlFactory(),
            bitmapLoader = BitmapLoader(
                executor,
                httpDownloader,
                BitmapDiscCache(applicationContext)
            )
        )
    }

    override fun setSearchQuery(searchQuery: String?) {
        searchQuery?.let {
            searchView.setText(it)
            searchView.setSelection(searchView.text.length)
        }
    }

    override fun setItems(items: List<ListItem>) {
        if (items == adapter.getItems()) {
            return
        }
        adapter.setItems(items)
        photosRecyclerView.postDelayed({
            loadMoreIfNeeded()
        }, 300)
    }

    private fun loadMoreIfNeeded() {
        if (layoutManager.findLastVisibleItemPosition() > adapter.itemCount - 1 - SPAN_COUNT) {
            presenter?.loadMorePhotos()
        }
    }

    private fun setupSearchQueryListener() {
        searchView.addTextChangedListener(object : TextWatcher {

            private val handler = Handler(Looper.getMainLooper())
            private var scheduledCallback: Runnable? = null

            override fun afterTextChanged(s: Editable?) {
                scheduledCallback?.let { callback ->
                    handler.removeCallbacks(callback)
                }
                val callback = Runnable { presenter?.onSearchQueryChanged(s.toString()) }
                handler.postDelayed(callback, INPUT_TEXT_DEBOUNCE_DURATION)
                scheduledCallback = callback
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
    }

    private fun calculateItemSize(): Int {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val spacing = resources.getDimensionPixelSize(R.dimen.photos_spacing_half) * 2
        return (displayMetrics.widthPixels - spacing) / SPAN_COUNT
    }

    private class CurrentTimeProvider : PhotosPaginator.CurrentTimeProvider {
        override fun getCurrentTimeMillis(): Long = System.currentTimeMillis()
    }
}

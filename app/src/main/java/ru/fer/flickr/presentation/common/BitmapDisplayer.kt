package ru.fer.flickr.presentation.common

import android.widget.ImageView
import ru.fer.flickr.R
import ru.fer.flickr.data.common.AsyncListener
import ru.fer.flickr.domain.common.BitmapLoader
import java.net.URL

class BitmapDisplayer(
    private val bitmapLoader: BitmapLoader
) {

    companion object {
        const val FADE_DURATION = 300L
        const val FADE_DURATION_FROM_CACHE = 200L
    }

    fun displayImage(url: URL, view: ImageView) {
        clear(view)

        bitmapLoader.loadBitmap(
            url = url,
            key = view,
            listener = object : AsyncListener<BitmapLoader.BitmapLoadingResult> {
                override fun onSuccess(result: BitmapLoader.BitmapLoadingResult) {
                    view.alpha = 0f
                    view.animate()
                        .alpha(1f)
                        .duration = if (result.fromCache) FADE_DURATION_FROM_CACHE else FADE_DURATION

                    view.setImageBitmap(result.bitmap)
                }

                override fun onError(exception: Exception) {
                    view.alpha = 0f
                    view.animate()
                        .alpha(1f)
                        .duration =
                        FADE_DURATION

                    view.setImageResource(R.drawable.ic_cross)
                }
            }
        )
    }

    private fun clear(view: ImageView) {
        bitmapLoader.clear(view)
        view.setImageDrawable(null)
        view.alpha = 0f
    }
}
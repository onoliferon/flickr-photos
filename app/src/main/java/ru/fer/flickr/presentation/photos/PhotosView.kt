package ru.fer.flickr.presentation.photos

import ru.fer.flickr.presentation.common.ListItem
import ru.fer.flickr.presentation.mvp.MvpView

interface PhotosView : MvpView {

    fun setItems(items: List<ListItem>)

    fun setSearchQuery(searchQuery: String?)
}
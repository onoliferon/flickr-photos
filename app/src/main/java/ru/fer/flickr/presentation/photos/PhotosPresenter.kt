package ru.fer.flickr.presentation.photos

import android.os.Bundle
import ru.fer.flickr.domain.photos.Photo
import ru.fer.flickr.domain.photos.PhotosPaginator
import ru.fer.flickr.domain.common.BitmapLoader
import ru.fer.flickr.domain.photos.PhotoUrlFactory
import ru.fer.flickr.presentation.common.ListItem
import ru.fer.flickr.presentation.mvp.MvpPresenter
import ru.fer.flickr.presentation.photos.delegates.EmptyResultDelegate
import ru.fer.flickr.presentation.photos.delegates.ErrorDelegate
import ru.fer.flickr.presentation.photos.delegates.LoadingDelegate
import ru.fer.flickr.presentation.photos.delegates.PhotoDelegate
import java.net.URL

class PhotosPresenter(
    private val photosPaginator: PhotosPaginator,
    private val photoUrlFactory: PhotoUrlFactory,
    private val bitmapLoader: BitmapLoader
) : MvpPresenter<PhotosView>() {

    private companion object {
        private const val STATE_SEARCH_QUERY = "search_query"
    }

    private val photosObserver: PhotosPaginator.PhotosObserver =
        object : PhotosPaginator.PhotosObserver {
            override fun onLoaded(photos: List<Photo>, allLoaded: Boolean) {
                view?.setItems(
                    if (allLoaded) {
                        if (photos.isEmpty()) {
                            listOf(createEmptyResultItem())
                        } else {
                            createPhotoItems(photos)
                        }
                    } else {
                        createPhotoItems(photos) + createLoadingItem()
                    }
                )
            }

            override fun onError(photos: List<Photo>, exception: Exception) {
                view?.setItems(
                    createPhotoItems(photos) + createErrorItem()
                )
            }
        }

    override fun onAttachView(view: PhotosView) {
        view.setSearchQuery(photosPaginator.getSearchQuery())
        photosPaginator.subscribe(photosObserver)
    }

    override fun onDetachView() {
        photosPaginator.unsubscribe(photosObserver)
        bitmapLoader.clearAll()
    }

    override fun onSaveInstanceState(state: Bundle) {
        state.putString(STATE_SEARCH_QUERY, photosPaginator.getSearchQuery())
    }

    override fun onRestoreInstanceState(state: Bundle) {
        val query = state.getString(STATE_SEARCH_QUERY)
        photosPaginator.resetWithSearchQuery(query)
    }

    fun getBitmapLoader(): BitmapLoader = bitmapLoader

    fun loadMorePhotos() {
        photosPaginator.loadMore()
    }

    fun retryLoadMorePhotos() {
        view?.setItems(createPhotoItems(photosPaginator.getLoadedPhotos()) + createLoadingItem())
        loadMorePhotos()
    }

    fun onSearchQueryChanged(query: String) {
        if (query == photosPaginator.getSearchQuery()) {
            return
        }
        view?.setItems(listOf(createLoadingItem()))
        photosPaginator.resetWithSearchQuery(query)
        photosPaginator.loadMore()
    }

    private fun createEmptyResultItem(): ListItem = EmptyResultDelegate.Item(listId = "empty")

    private fun createErrorItem(): ListItem = ErrorDelegate.Item(listId = "error")

    private fun createLoadingItem(): ListItem =
        LoadingDelegate.Item(listId = null) // Consider item always different for proper recyclerView animation

    private fun createPhotoItems(photos: List<Photo>): List<ListItem> {
        return photos.map { photo ->
            PhotoDelegate.Item(
                listId = photo.id,
                imageUrl = URL(photoUrlFactory.getPhotoUrl(photo)),
                title = photo.title
            )
        }
    }
}
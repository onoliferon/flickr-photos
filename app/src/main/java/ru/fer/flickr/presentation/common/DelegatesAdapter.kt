package ru.fer.flickr.presentation.common

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class DelegatesAdapter(
    private val delegates: List<AdapterDelegate<*, *>>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: MutableList<ListItem> = mutableListOf()

    fun getItems(): List<ListItem> = items

    fun setItems(items: List<ListItem>) {
        val diffResult = DiffUtil.calculateDiff(
            DiffCallback(
                this.items,
                items
            )
        )

        this.items.clear()
        this.items.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        delegates.forEachIndexed { index, delegate ->
            if (delegate.suitsFor(item)) {
                return index
            }
        }
        throw IllegalStateException("No adapter delegate found for item at $position")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegates[viewType].createViewHolder(parent)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        delegates.forEach { delegate ->
            if (delegate.suitsFor(item)) {
                @Suppress("UNCHECKED_CAST")
                (delegate as AdapterDelegate<ListItem, RecyclerView.ViewHolder>).bindViewHolder(
                    holder,
                    item
                )
                return
            }
        }
    }

    fun createSpanSizeLookup(spanCount: Int): GridLayoutManager.SpanSizeLookup {
        return object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val item = items[position]
                delegates.forEach { delegate ->
                    if (delegate.suitsFor(item)) {
                        return if (delegate is GridAdapterDelegate && delegate.takesFullWidth()) {
                            spanCount
                        } else {
                            1
                        }
                    }
                }
                return 1
            }
        }
    }

    private class DiffCallback(
        private val oldItems: List<ListItem>,
        private val newItems: List<ListItem>
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]
            return oldItem.listId == newItem.listId && oldItem.listId != null
        }

        override fun getOldListSize(): Int {
            return oldItems.size
        }

        override fun getNewListSize(): Int {
            return newItems.size
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldItems[oldItemPosition] == newItems[newItemPosition]
        }
    }
}

interface ListItem {
    /**
     * null value means that items should be considered always different
     */
    val listId: String?
}

interface AdapterDelegate<in I : ListItem, VH : RecyclerView.ViewHolder> {
    fun createViewHolder(parent: ViewGroup): VH

    fun bindViewHolder(holder: VH, item: I)

    fun suitsFor(item: ListItem): Boolean
}

interface GridAdapterDelegate {
    fun takesFullWidth(): Boolean
}
package ru.fer.flickr.presentation.photos.delegates

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.fer.flickr.R
import ru.fer.flickr.presentation.common.AdapterDelegate
import ru.fer.flickr.presentation.common.GridAdapterDelegate
import ru.fer.flickr.presentation.common.ListItem

class LoadingDelegate :
    AdapterDelegate<LoadingDelegate.Item, LoadingDelegate.ViewHolder>,
    GridAdapterDelegate {
    override fun createViewHolder(parent: ViewGroup): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_loading, parent, false)
        return ViewHolder(view)
    }

    override fun bindViewHolder(holder: ViewHolder, item: Item) {
    }

    override fun suitsFor(item: ListItem): Boolean = item is Item

    override fun takesFullWidth(): Boolean = true

    data class Item(
        override val listId: String? = null
    ) : ListItem

    class ViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view)
}
package ru.fer.flickr.data.common

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class HttpDownloader {

    companion object {
        private const val STRING_READ_BUFFER_SIZE = 500
        private const val CONNECTION_TIMEOUT_MILLIS = 5000
    }

    fun downloadUrl(url: URL): String {
        var connection: HttpsURLConnection? = null
        var inputStream: InputStream? = null
        val result: String
        try {
            connection = url.openConnection() as HttpsURLConnection
            inputStream = connect(connection)
            result = readStream(inputStream)
        } finally {
            inputStream?.close()
            connection?.disconnect()
        }
        return result
    }

    // TODO possible optimization: reuse bitmaps from some pool for decoding
    fun downloadImage(url: URL): Bitmap {
        val bitmap: Bitmap
        var connection: HttpURLConnection? = null
        var inputStream: InputStream? = null
        try {
            connection = url.openConnection() as HttpURLConnection
            inputStream = connect(connection)

            bitmap = BitmapFactory.decodeStream(inputStream)
        } finally {
            inputStream?.close()
            connection?.disconnect()
        }
        return bitmap
    }

    private fun connect(connection: HttpURLConnection): InputStream {
        connection.run {
            readTimeout = CONNECTION_TIMEOUT_MILLIS
            connectTimeout = CONNECTION_TIMEOUT_MILLIS
            connect()

            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw IOException("HTTP error code: $responseCode")
            }
        }
        return connection.inputStream
    }

    private fun readStream(stream: InputStream): String {
        val reader: Reader = InputStreamReader(stream, "UTF-8")
        val rawBuffer = CharArray(STRING_READ_BUFFER_SIZE)
        val buffer = StringBuffer()
        var readSize: Int = reader.read(rawBuffer)
        while (readSize != -1) {
            buffer.append(rawBuffer, 0, readSize)
            readSize = reader.read(rawBuffer)
        }
        return buffer.toString()
    }
}
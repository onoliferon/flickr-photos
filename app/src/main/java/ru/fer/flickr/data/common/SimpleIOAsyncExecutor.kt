package ru.fer.flickr.data.common

import android.os.Handler
import android.os.Looper
import android.os.Message
import java.util.concurrent.*
import java.util.concurrent.atomic.AtomicBoolean


class SimpleIOAsyncExecutor : AsyncExecutor {

    companion object {
        private const val MESSAGE_ID = 1
    }

    // Key - task lambda, value - cancellation info
    private val tasks: MutableMap<Any, AsyncExecutor.CancellationInfo> = mutableMapOf()

    private val mainThreadHandler = object : Handler(Looper.getMainLooper()) {

        override fun handleMessage(msg: Message) {
            @Suppress("UNCHECKED_CAST")
            val result = msg.obj as ExecutionResult<Any>
            val info = tasks[result.task]!! // Task must exist
            tasks.remove(result.task)
            if (info.isCancelled()) {
                // Don't notify callback if job is cancelled
                return
            }

            if (result.error != null) {
                result.callback.onError(result.error)
            } else if (result.data != null) {
                result.callback.onSuccess(result.data)
            }
        }
    }

    // Don't use too many threads as this can cause OOM
    private val executorService = Executors.newFixedThreadPool(16)

    override fun <T> execute(
        task: (AsyncExecutor.CancellationInfo) -> T,
        listener: AsyncListener<T>
    ): AsyncExecutor.Job {

        val cancellationInfo = SimpleCancellationInfo()
        val future = executorService.submit {
            var result: T? = null
            var exception: Exception? = null
            try {
                result = task(cancellationInfo)
            } catch (e: Exception) {
                exception = e
            }

            mainThreadHandler.obtainMessage(
                MESSAGE_ID, ExecutionResult(
                    task,
                    result,
                    exception,
                    listener
                )
            ).sendToTarget()
        }

        val job = SimpleJob(future, cancellationInfo)
        tasks[task] = cancellationInfo
        return job
    }

    private data class ExecutionResult<T>(
        val task: Any,
        val data: T?,
        val error: Exception?,
        val callback: AsyncListener<in T>
    )

    private class SimpleJob(
        val future: Future<*>,
        val cancellationInfo: SimpleCancellationInfo
    ) : AsyncExecutor.Job {
        override fun cancel() {
            cancellationInfo.cancelled.set(true)
            future.cancel(true)
        }
    }

    private class SimpleCancellationInfo : AsyncExecutor.CancellationInfo {

        val cancelled: AtomicBoolean = AtomicBoolean(false)

        override fun isCancelled(): Boolean {
            return cancelled.get()
        }
    }
}
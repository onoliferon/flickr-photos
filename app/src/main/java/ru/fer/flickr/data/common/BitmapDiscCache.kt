package ru.fer.flickr.data.common

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.os.Environment.isExternalStorageRemovable
import java.io.*

// TODO abstract this class from Android framework and cover with tests
class BitmapDiscCache(private val context: Context) {

    companion object {
        private val CACHE_KEY_CLEANUP_REGEX = Regex("[^a-zA-Z0-9]")
        private const val CACHE_DIR_NAME = "photos"
        private const val CACHE_SIZE = 25 * 1024 * 1024 // Might be set from some settings
    }

    fun get(key: String): Bitmap? {
        val cacheDir = getDiskCacheDir()
        val file = File(cacheDir, getFileName(key))

        if (!file.exists()) {
            return null
        }

        var inputStream: FileInputStream? = null
        var bitmap: Bitmap? = null
        try {
            inputStream = FileInputStream(file)
            bitmap = BitmapFactory.decodeStream(inputStream)
        } catch (e: FileNotFoundException) {
            // Ignore
        } catch (e: IOException) {
            // Ignore
        } finally {
            inputStream?.close()
        }

        return bitmap
    }

    @Synchronized
    fun put(bitmap: Bitmap, key: String) {
        val cacheDir = getDiskCacheDir()
        val file = File(cacheDir, getFileName(key))
        var outputStream: FileOutputStream? = null
        try {
            outputStream = FileOutputStream(file)
            bitmap.compress(
                Bitmap.CompressFormat.JPEG,
                100, outputStream
            )

            file.setLastModified(System.currentTimeMillis())
        } catch (e: FileNotFoundException) {
            // Ignore
        } catch (e: IOException) {
            // Ignore
        } finally {
            outputStream?.flush()
            outputStream?.close()
        }

        // Remove old images if space limit was exceeded
        try {
            cleanupCache()
        } catch (e: FileNotFoundException) {
            // Ignore
        } catch (e: IOException) {
            // Ignore
        }
    }

    // TODO make it LRU
    //  (some special metainfo file with usage time is probably required)
    //  (or to store last usage time as a part of file name)
    private fun cleanupCache() {
        val cacheDir = getDiskCacheDir()

        val maxCacheSize = CACHE_SIZE

        val files = cacheDir.listFiles() ?: return
        val cacheSize = files
            .map { it.length() }
            .fold(0L, { acc, length -> acc + length })

        // Delete the oldest files
        if (cacheSize > maxCacheSize) {
            val limitExceed = cacheSize - maxCacheSize
            files.sortBy { it.lastModified() }

            var deletedSize = 0L
            files.forEach { file ->
                deletedSize += file.length()
                file.delete()
                if (deletedSize >= limitExceed) {
                    return
                }
            }
        }
    }

    private fun getFileName(key: String) = "${key.replace(CACHE_KEY_CLEANUP_REGEX, "")}.jpg"

    // Creates a unique subdirectory of the designated app cache directory. Tries to use external
    // but if not mounted, falls back on internal storage.
    private fun getDiskCacheDir(): File {
        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir
        val cachePath = if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()
            || !isExternalStorageRemovable()
        ) {
            context.externalCacheDir?.path ?: context.cacheDir.path
        } else {
            context.cacheDir.path
        }

        val result = File(cachePath + File.separator + CACHE_DIR_NAME)

        if (!result.exists()) {
            result.mkdir()
        }
        return result
    }
}
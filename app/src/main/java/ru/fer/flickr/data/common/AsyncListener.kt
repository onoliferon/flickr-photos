package ru.fer.flickr.data.common

import java.lang.Exception

interface AsyncListener<T> {
    fun onSuccess(result: T)

    fun onError(exception: Exception)
}

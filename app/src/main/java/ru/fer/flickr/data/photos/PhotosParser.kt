package ru.fer.flickr.data.photos

import org.json.JSONObject
import ru.fer.flickr.domain.photos.LoadedPhotos
import ru.fer.flickr.domain.photos.Photo

class PhotosParser {
    fun parse(json: String): LoadedPhotos {
        val images = mutableListOf<Photo>()

        val root = JSONObject(json)
        val photosObj: JSONObject = root.getJSONObject("photos")
        val imagesArr = photosObj.getJSONArray("photo")

        for (i in 0 until imagesArr.length()) {
            val imageObj = imagesArr.getJSONObject(i)
            images.add(
                Photo(
                    id = imageObj.getString("id"),
                    server = imageObj.getString("server"),
                    secret = imageObj.getString("secret"),
                    farm = imageObj.getInt("farm"),
                    title = imageObj.getString("title")
                )
            )
        }

        val currentPage = photosObj.getInt("page")
        val totalPages = photosObj.getInt("pages")

        return LoadedPhotos(
            photos = images,
            allLoaded = currentPage >= totalPages
        )
    }
}
package ru.fer.flickr.data.common

interface AsyncExecutor {

    fun <T> execute(task: (CancellationInfo) -> T, listener: AsyncListener<T>): Job

    interface Job {
        fun cancel()
    }

    interface CancellationInfo {
        fun isCancelled(): Boolean
    }
}
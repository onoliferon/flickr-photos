package ru.fer.flickr.data.photos

import ru.fer.flickr.data.common.AsyncExecutor
import ru.fer.flickr.data.common.AsyncListener
import ru.fer.flickr.data.common.HttpDownloader
import ru.fer.flickr.domain.photos.LoadedPhotos
import java.net.URL

class PhotosRepository(
    private val asyncExecutor: AsyncExecutor,
    private val httpDownloader: HttpDownloader,
    private val photosParser: PhotosParser
) {

    private companion object {
        private const val BASE_URL = "https://api.flickr.com/services/rest"
        private const val METHOD = "flickr.photos.search"
        private const val API_KEY = "3e7cc266ae2b0e0d78e279ce8e361736"
    }

    private var currentJob: AsyncExecutor.Job? = null

    fun loadPhotos(
        page: Int,
        limit: Int,
        searchString: String?,
        maxUploadDateSeconds: Long, // To avoid duplicates when photo is added during some page loading
        listener: AsyncListener<LoadedPhotos>
    ) : AsyncExecutor.Job {

        currentJob?.cancel()
        return asyncExecutor.execute(
            task = {
                if (searchString.isNullOrEmpty()) {
                    return@execute LoadedPhotos(
                        photos = emptyList(),
                        allLoaded = true
                    )
                }
                val json = httpDownloader.downloadUrl(
                    createUrl(
                        page = page,
                        limit = limit,
                        maxUploadDate = maxUploadDateSeconds,
                        searchString = searchString
                    )
                )
                photosParser.parse(json)
            },
            listener = listener
        )
    }

    private fun createUrl(page: Int, limit: Int, maxUploadDate: Long, searchString: String?): URL {
        return URL("$BASE_URL/?method=$METHOD&api_key=$API_KEY&format=json&nojsoncallback=1&safe_search=1&page=$page&per_page=$limit&text=$searchString&max_upload_date=$maxUploadDate")
    }
}
package ru.fer.flickr

import ru.fer.flickr.data.common.AsyncExecutor
import ru.fer.flickr.data.common.AsyncListener

class ImmediateExecutor : AsyncExecutor {

    var jobToReturn: AsyncExecutor.Job? = null
    var neverCallListener = false

    private val defaultCancellationInfo = object : AsyncExecutor.CancellationInfo {
        override fun isCancelled(): Boolean = false
    }

    override fun <T> execute(
        task: (AsyncExecutor.CancellationInfo) -> T,
        listener: AsyncListener<T>
    ): AsyncExecutor.Job {
        try {
            val result = task(defaultCancellationInfo)
            if (!neverCallListener) {
                listener.onSuccess(result)
            }
        } catch (exception: Exception) {
            if (!neverCallListener) {
                listener.onError(exception)
            }
        }
        return jobToReturn ?: object : AsyncExecutor.Job {
            override fun cancel() {
            }
        }
    }
}
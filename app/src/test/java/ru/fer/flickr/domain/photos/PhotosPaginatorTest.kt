package ru.fer.flickr.domain.photos

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.internal.verification.Times
import ru.fer.flickr.anyNullable
import ru.fer.flickr.argumentCaptor
import ru.fer.flickr.data.common.AsyncExecutor
import ru.fer.flickr.data.common.AsyncListener
import ru.fer.flickr.data.photos.PhotosRepository

class PhotosPaginatorTest {

    private val photosRepository = mock(PhotosRepository::class.java)
    private val photosObserver = mock(PhotosPaginator.PhotosObserver::class.java)
    private val currentTimeProvider = mock(PhotosPaginator.CurrentTimeProvider::class.java)

    private lateinit var photosPaginator: PhotosPaginator

    private val initialQuery = "query"
    private val limitPerPage = 6

    private val testCurrentTime = 120000000L
    private val expectedMaxDate = testCurrentTime / 1000 - 20 * 60

    @Before
    fun setUp() {
        photosPaginator = PhotosPaginator(
            photosRepository = photosRepository,
            currentTimeProvider = currentTimeProvider,
            limitPerPage = limitPerPage,
            initialQuery = initialQuery
        )

        `when`(currentTimeProvider.getCurrentTimeMillis()).thenReturn(testCurrentTime)
    }

    @Test
    fun `load two pages`() {
        val firstPage = createPhotos(count = limitPerPage, fromIndex = 0)
        val secondPage = createPhotos(count = 3, fromIndex = 3)

        photosPaginator.subscribe(photosObserver)

        photosPaginator.loadMore()

        val firstCaptor = argumentCaptor<AsyncListener<LoadedPhotos>>()
        verify(photosRepository)
            .loadPhotos(
                page = eq(1),
                limit = eq(limitPerPage),
                searchString = eq(initialQuery),
                maxUploadDateSeconds = eq(expectedMaxDate),
                listener = firstCaptor.capture()
            )

        firstCaptor.firstValue.onSuccess(LoadedPhotos(firstPage, allLoaded = false))

        photosPaginator.loadMore()

        val secondCaptor = argumentCaptor<AsyncListener<LoadedPhotos>>()
        verify(photosRepository)
            .loadPhotos(
                page = eq(2),
                limit = eq(limitPerPage),
                searchString = eq(initialQuery),
                maxUploadDateSeconds = eq(expectedMaxDate),
                listener = secondCaptor.capture()
            )

        secondCaptor.firstValue.onSuccess(LoadedPhotos(secondPage, allLoaded = true))

        verify(photosObserver).onLoaded(emptyList(), false)
        verify(photosObserver).onLoaded(firstPage, false)
        verify(photosObserver).onLoaded(firstPage + secondPage, allLoaded = true)
        verify(currentTimeProvider).getCurrentTimeMillis()
    }

    @Test
    fun `initial loading error`() {
        val exception = IllegalStateException()
        photosPaginator.subscribe(photosObserver)

        photosPaginator.loadMore()

        val firstCaptor = argumentCaptor<AsyncListener<LoadedPhotos>>()
        verify(photosRepository)
            .loadPhotos(
                page = eq(1),
                limit = eq(limitPerPage),
                searchString = eq(initialQuery),
                maxUploadDateSeconds = eq(expectedMaxDate),
                listener = firstCaptor.capture()
            )

        firstCaptor.firstValue.onError(exception)

        verify(photosObserver).onLoaded(emptyList(), false)
        verify(photosObserver).onError(emptyList(), exception)
        verify(currentTimeProvider).getCurrentTimeMillis()
    }

    @Test
    fun `second page loading error`() {
        val firstPage = createPhotos(count = limitPerPage, fromIndex = 0)
        val exception = IllegalStateException()

        photosPaginator.subscribe(photosObserver)

        photosPaginator.loadMore()

        val firstCaptor = argumentCaptor<AsyncListener<LoadedPhotos>>()
        verify(photosRepository)
            .loadPhotos(
                page = eq(1),
                limit = eq(limitPerPage),
                searchString = eq(initialQuery),
                maxUploadDateSeconds = eq(expectedMaxDate),
                listener = firstCaptor.capture()
            )

        firstCaptor.firstValue.onSuccess(LoadedPhotos(firstPage, allLoaded = false))

        photosPaginator.loadMore()

        val secondCaptor = argumentCaptor<AsyncListener<LoadedPhotos>>()
        verify(photosRepository)
            .loadPhotos(
                page = eq(2),
                limit = eq(limitPerPage),
                searchString = eq(initialQuery),
                maxUploadDateSeconds = eq(expectedMaxDate),
                listener = secondCaptor.capture()
            )

        secondCaptor.firstValue.onError(exception)

        verify(photosObserver).onLoaded(emptyList(), false)
        verify(photosObserver).onLoaded(firstPage, false)
        verify(photosObserver).onError(firstPage, exception)
        verify(currentTimeProvider).getCurrentTimeMillis()
    }

    @Test
    fun `query change cancels current loading`() {
        val currentLoadingJob = mock(AsyncExecutor.Job::class.java)

        `when`(
            photosRepository.loadPhotos(
                page = eq(1),
                limit = eq(limitPerPage),
                searchString = eq(initialQuery),
                maxUploadDateSeconds = eq(expectedMaxDate),
                listener = anyNullable()
            )
        ).thenReturn(currentLoadingJob)

        photosPaginator.subscribe(photosObserver)
        photosPaginator.loadMore()

        photosPaginator.resetWithSearchQuery("new query")

        verify(currentLoadingJob).cancel()
        verify(currentTimeProvider, Times(2)).getCurrentTimeMillis()
    }

    private fun createPhotos(count: Int, fromIndex: Int = 0) =
        (fromIndex until fromIndex + count).map { index ->
            Photo(
                id = "id$index",
                title = "title$index",
                farm = index,
                server = "server$index",
                secret = "secret$index"
            )
        }
}
package ru.fer.flickr.domain.common

import android.graphics.Bitmap
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import ru.fer.flickr.ImmediateExecutor
import ru.fer.flickr.data.common.AsyncExecutor
import ru.fer.flickr.data.common.AsyncListener
import ru.fer.flickr.data.common.BitmapDiscCache
import ru.fer.flickr.data.common.HttpDownloader
import java.net.URL

class BitmapLoaderTest {

    private val asyncExecutor: ImmediateExecutor = ImmediateExecutor()

    private val httpDownloader = mock(HttpDownloader::class.java)
    private val bitmapDiscCache = mock(BitmapDiscCache::class.java)
    @Suppress("UNCHECKED_CAST")
    private val listener = mock(AsyncListener::class.java) as AsyncListener<BitmapLoader.BitmapLoadingResult>

    private lateinit var bitmapLoader: BitmapLoader

    private val testUrl = URL("http://test.com")
    private val testKey = "key"

    private val testBitmap: Bitmap = mock(Bitmap::class.java)
    private val testJob: AsyncExecutor.Job = mock(AsyncExecutor.Job::class.java)

    @Before
    fun setUp() {
        bitmapLoader = BitmapLoader(
            asyncExecutor = asyncExecutor,
            httpDownloader = httpDownloader,
            discCache = bitmapDiscCache
        )
        asyncExecutor.jobToReturn = testJob
    }

    @Test
    fun `load image success no cache`() {
        `when`(bitmapDiscCache.get(testUrl.toString())).thenReturn(null)
        `when`(httpDownloader.downloadImage(testUrl)).thenReturn(testBitmap)

        bitmapLoader.loadBitmap(url = testUrl, key = testKey, listener = listener)

        verify(bitmapDiscCache).get(testUrl.toString())
        verify(bitmapDiscCache).put(testBitmap, testUrl.toString())
        verify(httpDownloader).downloadImage(testUrl)
        verifyNoMoreInteractions(bitmapDiscCache)
        verifyNoMoreInteractions(httpDownloader)
        verify(listener).onSuccess(
            BitmapLoader.BitmapLoadingResult(
                bitmap = testBitmap,
                fromCache = false
            )
        )
    }

    @Test
    fun `load image success from cache`() {
        `when`(bitmapDiscCache.get(testUrl.toString())).thenReturn(testBitmap)

        bitmapLoader.loadBitmap(url = testUrl, key = testKey, listener = listener)

        verify(bitmapDiscCache).get(testUrl.toString())
        verifyNoMoreInteractions(bitmapDiscCache)
        verifyNoMoreInteractions(httpDownloader)
        verify(listener).onSuccess(
            BitmapLoader.BitmapLoadingResult(
                bitmap = testBitmap,
                fromCache = true
            )
        )
    }

    @Test
    fun `load image error getting from cache`() {
        val exception = IllegalStateException("test")
        `when`(bitmapDiscCache.get(testUrl.toString())).thenThrow(exception)

        bitmapLoader.loadBitmap(url = testUrl, key = testKey, listener = listener)

        verify(bitmapDiscCache).get(testUrl.toString())
        verifyNoMoreInteractions(bitmapDiscCache)
        verifyNoMoreInteractions(httpDownloader)
        verify(listener).onError(exception)
    }

    @Test
    fun `load image error getting from network`() {
        val exception = IllegalStateException("test")
        `when`(bitmapDiscCache.get(testUrl.toString())).thenReturn(null)
        `when`(httpDownloader.downloadImage(testUrl)).thenThrow(exception)

        bitmapLoader.loadBitmap(url = testUrl, key = testKey, listener = listener)

        verify(bitmapDiscCache).get(testUrl.toString())
        verifyNoMoreInteractions(bitmapDiscCache)
        verify(httpDownloader).downloadImage(testUrl)
        verifyNoMoreInteractions(httpDownloader)
        verify(listener).onError(exception)
    }

    @Test
    fun `cancel previous job with the same key`() {
        val testUrl2 = URL("http://test2.com")
        `when`(bitmapDiscCache.get(testUrl.toString())).thenReturn(testBitmap)
        `when`(bitmapDiscCache.get(testUrl2.toString())).thenReturn(testBitmap)

        val firstJob = mock(AsyncExecutor.Job::class.java)

        asyncExecutor.jobToReturn = firstJob
        asyncExecutor.neverCallListener = true
        bitmapLoader.loadBitmap(url = testUrl, key = testKey, listener = listener)

        asyncExecutor.jobToReturn = null
        asyncExecutor.neverCallListener = false
        bitmapLoader.loadBitmap(url = testUrl2, key = testKey, listener = listener)

        verify(firstJob).cancel()

        verify(bitmapDiscCache).get(testUrl.toString())
        verify(bitmapDiscCache).get(testUrl2.toString())
        verifyNoMoreInteractions(bitmapDiscCache)
        verifyNoMoreInteractions(httpDownloader)
        verify(listener).onSuccess(
            BitmapLoader.BitmapLoadingResult(
                bitmap = testBitmap,
                fromCache = true
            )
        )
    }
}
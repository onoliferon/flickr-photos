package ru.fer.flickr.presentation.photos

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*
import org.mockito.internal.verification.Times
import org.mockito.invocation.InvocationOnMock
import ru.fer.flickr.anyNullable
import ru.fer.flickr.argumentCaptor
import ru.fer.flickr.domain.common.BitmapLoader
import ru.fer.flickr.domain.photos.Photo
import ru.fer.flickr.domain.photos.PhotoUrlFactory
import ru.fer.flickr.domain.photos.PhotosPaginator
import ru.fer.flickr.presentation.common.ListItem
import ru.fer.flickr.presentation.photos.delegates.EmptyResultDelegate
import ru.fer.flickr.presentation.photos.delegates.ErrorDelegate
import ru.fer.flickr.presentation.photos.delegates.LoadingDelegate
import ru.fer.flickr.presentation.photos.delegates.PhotoDelegate
import java.net.URL

class PhotosPresenterTest {

    private val photosPaginator = mock(PhotosPaginator::class.java)
    private val bitmapLoader = mock(BitmapLoader::class.java)
    private val photoUrlFactory = mock(PhotoUrlFactory::class.java)
    private val view = mock(PhotosView::class.java)

    private lateinit var presenter: PhotosPresenter

    @Before
    fun setUp() {
        presenter = PhotosPresenter(
            photosPaginator = photosPaginator,
            bitmapLoader = bitmapLoader,
            photoUrlFactory = photoUrlFactory
        )
    }

    @Test
    fun `initial subscription - all loaded`() {
        val (photos, items) = createStubPhotos()
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onLoaded(photos, allLoaded = true)
        }

        presenter.attachView(view)

        verify(view).setItems(
            items
        )
    }

    @Test
    fun `initial subscription - not all loaded`() {
        val (photos, items) = createStubPhotos()
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onLoaded(photos, allLoaded = false)
        }

        presenter.attachView(view)

        verify(view).setItems(
            items + LoadingDelegate.Item()
        )
    }

    @Test
    fun `initial subscription - error`() {
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onError(emptyList(), exception = IllegalStateException())
        }

        presenter.attachView(view)

        verify(view).setItems(
            listOf(ErrorDelegate.Item("error"))
        )
    }

    @Test
    fun `load more - success all loaded`() {
        val (photos, items) = createStubPhotos(count = 3)
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onLoaded(photos, allLoaded = true)
        }

        presenter.attachView(view)
        presenter.loadMorePhotos()

        verify(photosPaginator).loadMore()
        verify(view).setItems(items)
    }

    @Test
    fun `load more - success not all loaded`() {
        val (photos, items) = createStubPhotos(count = 3)
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onLoaded(photos, allLoaded = false)
        }

        presenter.attachView(view)
        presenter.loadMorePhotos()

        verify(photosPaginator).loadMore()
        verify(view).setItems(items + LoadingDelegate.Item())
    }

    @Test
    fun `load more - error`() {
        val (photos, items) = createStubPhotos(count = 3)
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onError(photos, exception = IllegalStateException())
        }

        presenter.attachView(view)
        presenter.loadMorePhotos()

        verify(view).setItems(
            items + ErrorDelegate.Item("error")
        )
    }

    @Test
    fun `load more - empty`() {
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onLoaded(emptyList(), allLoaded = true)
        }

        presenter.attachView(view)
        presenter.loadMorePhotos()

        verify(photosPaginator).loadMore()
        verify(view).setItems(listOf(EmptyResultDelegate.Item("empty")))
    }

    @Test
    fun `retry - success`() {
        val (photos, items) = createStubPhotos(count = 3)
        val (newPhotos, newItems) = createStubPhotos(count = 6)
        `when`(photosPaginator.getLoadedPhotos()).thenReturn(photos)
        `when`(photosPaginator.subscribe(anyNullable())).thenAnswer {
            it.firstArgAsPhotosObserver().onLoaded(newPhotos, allLoaded = true)
        }

        presenter.attachView(view)
        presenter.retryLoadMorePhotos()

        verify(photosPaginator).loadMore()
        verify(view).setItems(
            items + LoadingDelegate.Item()
        )
        verify(view).setItems(
            newItems
        )
    }

    @Test
    fun `initial subscription and then load more`() {
        val (photos, items) = createStubPhotos(count = 3)
        val (newPhotos, newItems) = createStubPhotos(count = 6)

        presenter.attachView(view)

        val captor = argumentCaptor<PhotosPaginator.PhotosObserver>()
        verify(photosPaginator).subscribe(captor.capture())
        captor.firstValue.onLoaded(photos, allLoaded = false)

        presenter.loadMorePhotos()
        captor.firstValue.onLoaded(newPhotos, allLoaded = true)

        verify(photosPaginator).loadMore()
        verify(view).setItems(
            items + LoadingDelegate.Item()
        )
        verify(view).setItems(
            newItems
        )
    }

    @Test
    fun `initial subscription - restore search query`() {
        `when`(photosPaginator.getSearchQuery()).thenReturn("old query")
        presenter.attachView(view)

        verify(photosPaginator).getSearchQuery()
        verify(view).setSearchQuery("old query")
    }

    @Test
    fun `search query - changed`() {
        presenter.attachView(view)

        `when`(photosPaginator.getSearchQuery()).thenReturn("old query")

        presenter.onSearchQueryChanged("new query")

        verify(photosPaginator).resetWithSearchQuery("new query")
        verify(photosPaginator).loadMore()
        verify(view).setItems(listOf(LoadingDelegate.Item()))
    }

    @Test
    fun `search query - not changed`() {
        `when`(photosPaginator.getSearchQuery()).thenReturn("old query")

        presenter.attachView(view)

        presenter.onSearchQueryChanged("old query")

        verify(photosPaginator, Times(2)).getSearchQuery()
        verify(photosPaginator).subscribe(anyNullable())
        verifyNoMoreInteractions(photosPaginator)
        verify(view).setSearchQuery("old query")
        verifyNoMoreInteractions(view)
    }

    @Test
    fun `detach view`() {
        presenter.attachView(view)

        val captor = argumentCaptor<PhotosPaginator.PhotosObserver>()
        verify(photosPaginator).subscribe(captor.capture())

        presenter.detachView()
        verify(photosPaginator).unsubscribe(captor.firstValue)
        verify(bitmapLoader).clearAll()
    }

    private fun createStubPhotos(count: Int = 3): Pair<List<Photo>, List<ListItem>> {
        val photos = (0 until count).map { index ->
            Photo(
                id = "id$index",
                title = "title$index",
                farm = index,
                server = "server$index",
                secret = "secret$index"
            )
        }
        val items = photos.map { photo ->
            val url = "http://test${photo.id}.com"
            `when`(photoUrlFactory.getPhotoUrl(photo)).thenReturn(url)
            PhotoDelegate.Item(
                listId = photo.id,
                imageUrl = URL(url),
                title = photo.title
            )
        }
        return photos to items
    }

    private fun InvocationOnMock.firstArgAsPhotosObserver(): PhotosPaginator.PhotosObserver {
        return getArgument(0) as PhotosPaginator.PhotosObserver
    }
}